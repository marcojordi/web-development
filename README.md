# Web Development
This repository contains basic web development projects programmed with Python 3.6.xx using [Flask](https://github.com/pallets/flask) and [Django](https://www.djangoproject.com/)

- Basic Example with Flask, three different pages (Home, Table, Plot) 
- Basic Example with Django, basic polls app from [Django Docs](https://docs.djangoproject.com/en/2.1/intro/)


