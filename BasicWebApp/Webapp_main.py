# -*- coding: utf-8 -*-
###############################################################################
#
# Import section
###############################################################################
import traceback
import numpy as np
import pandas as pd
#flask
from flask import Flask, render_template
#bokeh
from bokeh.plotting import figure
from bokeh.embed import components
from bokeh.resources import INLINE
from bokeh.util.string import encode_utf8
###############################################################################
#
# Header
###############################################################################
__author__ = ['Marco Jordi']
__maintainer__ = ['Marco Jordi']
__email__ = ['marco.jordi@bfh.ch']

"""
Created on Fri Sept 21 13:50:22 2018

@author: Marco Jordi
"""
###############################################################################
###############################################################################

app = Flask(__name__)

def make_line_plot():
    """Makes a basic line plot
    """
    plot = figure(plot_width=800, plot_height=400, toolbar_location="right")
    xdata = np.linspace(0,10,100)
    ydata = [x*x for x in xdata]
    yydata = [np.sqrt(x)*10 for x in xdata]
    yyydata = [20*np.sin(3*x)+20 for x in xdata]
	
    plot.line(xdata, ydata, legend="Quadratic")
    plot.line(xdata, yydata, color="yellowgreen", legend="Root")
    plot.line(xdata, yyydata, color="orange", legend="Sinus")
	
    plot.xaxis.axis_label = 'X label'
    plot.yaxis.axis_label = 'Y label'
    plot.legend.location = "top_left"
    plot.min_border_top = 50
    plot.min_border_bottom = 80
    plot.min_border_right = 65
    plot.min_border_left = 70
	
    return plot
	
def make_table():
	"""Makes a Table
	"""
	table = pd.read_excel('static/data/data.xlsx')
	
	return table

@app.route("/")  
def home():
	return render_template('home.html', script="", resources="")
	
@app.route('/table/')
def table():
    table = make_table()
    return render_template('table.html',table=table.to_html(), script="", resources="")
	
@app.route('/plot')
def plot():
    plot = make_line_plot()
	
    resources = INLINE.render()

    script, div = components(plot)
    html = render_template('plot.html',text="Plot some fancy data", script=script, div=div, resources=resources)
	
    return encode_utf8(html)


if __name__ == "__main__":

    try:

        app.run(debug = True)
        
    except:
        traceback.print_exc()


   